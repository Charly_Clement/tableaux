<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Faites 2 tableaux : un avec 5 prénoms de garçons et un avec 5 prénoms de filles
    // En fonction du chiffre que choisit $x, afficher les 5 prénoms de garçon ou sinon
    // les 5 prénoms de filles
    
    $x = rand(0,1);
    echo $x.'<br>';
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php
  
        $garcon = ['Zeus', 'Poséidon', 'Arès', 'Apollon', 'Hermès'];
        $fille = ['Aphrodite', 'Héra', 'Métis', 'Thémis' , 'Artémis'];

        if ($x == 0) {
            foreach ($garcon as $valeur)
            echo $valeur.' ';
        }else {
            foreach ($fille as $valeur)
            echo $valeur.' ';
        }

    
    ?>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>