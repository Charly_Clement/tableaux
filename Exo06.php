<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Créer un tableau avec tous les mois de l'année. Faites une boucle pour afficher :
    // Janvier est le mois numéro 1.
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php
        $mois = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août',
                    'Septembre', 'Octobre', 'Novembre', 'Décembre',];
    
        for ($x = 0;$x < count ($mois); $x++) {
            echo $mois[$x] . ' est le mois numéro ' . ($x+1) . '<br>';
        }


    ?> 
    <!-- écrire le code avant ce commentaire -->

</body>
</html>