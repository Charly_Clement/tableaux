<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

<?php

// Créer un tableau associatif pour la recette des crêpes.
// Associez les ingrédients à leur quantités. 
// Afficher ensuite la liste des ingrédients compléte. 
// Farine, sel, huile, sucre, lait, oeufs, goutte et le temps de repos de la pâte. 


?>
<h1>Recette des crêpes</h1>
<!-- écrire le code après ce commentaire -->
<?php
    $recette = ['Farine: ' => '1 grammes', 'Sel: ' => '7 cuillères à soupe',
                'Huile: ' => '10 litres', 'Sucre: ' => '20 kilos', 'Lait: ' => '30 litres',
                'Oeufs: ' => '3 entier', 'Rhum: ' => 'A volonté',
                'Temps de repos: ' => '1 semaine'];

    foreach ($recette as $key => $valeur) {
        echo $key.=$valeur.'<br>';
    }

    


?>

<!-- écrire le code avant ce commentaire -->

</body>
</html>